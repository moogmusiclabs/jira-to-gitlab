#!/usr/bin/python3

# Copyright 2019 Moog Music Inc. (https://www.moogmusic.com)
# Distributed under the MIT license, see LICENSE file

import requests
from requests.auth import HTTPBasicAuth
import re
from io import BytesIO

# This script BULK DELETES all issues from a GitLab project WITHOUT ANY CONFIRMATION
# BE VERY CAREFUL when using this script, you will not be able to retrieve the issues afterwards!!!

########################################
#
# FILL THESE IN FOR YOUR ACCOUNTS
#
# this token will be used whenever the API is invoked and
# the script will be unable to match the Jira's author of the comment / attachment / issue
# this identity will be used instead.
GITLAB_TOKEN = 'gitlab-token'
# the group in GitLab that the users and project belongs to
GITLAB_GROUPNAME = 'group-name'
# the project in GitLab that you are importing issues to.
GITLAB_PROJECTNAME = 'project-name'
# the numeric project ID. If you don't know it, the script will search for it
# based on the project name.
GITLAB_PROJECT_ID = None
#
########################################

# set this to false if Gitlab is using a self-signed certificate.
VERIFY_SSL_CERTIFICATE = True

GITLAB_URL = 'https://gitlab.com/'
GITLAB_PROJECT = '%s/%s' % (GITLAB_GROUPNAME, GITLAB_PROJECTNAME)

if not GITLAB_PROJECT_ID:
    # find out the ID of the project.
    project_request = requests.get(
        GITLAB_URL + 'api/v4/projects?membership=true&simple=true&search=%s' % GITLAB_PROJECTNAME,
        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
        verify=VERIFY_SSL_CERTIFICATE
    )
    for project in project_request.json():
        if project['path_with_namespace'] == GITLAB_PROJECT:
            GITLAB_PROJECT_ID = project['id']
            break

if not GITLAB_PROJECT_ID:
    raise Exception('Unable to find %s in gitlab!' % GITLAB_PROJECT)

while True:
    gitlab_issues = requests.get(
        GITLAB_URL + 'api/v4/projects/%s/issues' % GITLAB_PROJECT_ID,
        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
        verify=VERIFY_SSL_CERTIFICATE
    )

    if len(gitlab_issues.json()) == 0:
        break;

    for issue in gitlab_issues.json():
        print(issue)

        gl_issue_id = issue["iid"]

        gitlab_delete = requests.delete(
            GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, gl_issue_id),
            headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
            verify=VERIFY_SSL_CERTIFICATE
        )

        print ("> deleted issue %s : %d" % (gl_issue_id, gitlab_delete.status_code))
 
        print('')