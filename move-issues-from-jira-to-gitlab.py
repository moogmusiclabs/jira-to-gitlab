#!/usr/bin/python3

# Copyright 2019 Moog Music Inc. (https://www.moogmusic.com)
# Distributed under the MIT license, see LICENSE file

import requests
from requests.auth import HTTPBasicAuth
import re
from io import BytesIO

# this script was adapted from https://gist.github.com/toudi/67d775066334dc024c24

########################################
#
# FILL THESE IN FOR YOUR ACCOUNTS
#
JIRA_URL = 'https://my-org.atlassian.net/'
JIRA_ACCOUNT = ('jira-username', 'jira-password')
# the Jira project ID (short)
JIRA_PROJECT = 'PROJ'
# this token will be used whenever the API is invoked and
# the script will be unable to match the Jira's author of the comment / attachment / issue
# this identity will be used instead.
GITLAB_TOKEN = 'gitlab-token'
# the group in GitLab that the users and project belongs to
GITLAB_GROUPNAME = 'group-name'
# the project in GitLab that you are importing issues to.
GITLAB_PROJECTNAME = 'project-name'
# the numeric project ID. If you don't know it, the script will search for it
# based on the project name.
GITLAB_PROJECT_ID = None

# IMPORTANT !!!
# make sure that user (in GitLab) has access to the project you are trying to
# import into. Otherwise the API request will fail.
# if you want dates and times to be correct, make sure every user is (temporarily) admin
GITLAB_USER_TOKENS = {
    'jira-username1' : 'gitlab-private-token1', 
    'jira-username2' : 'gitlab-private-token2', 
    'jira-username3' : 'gitlab-private-token3'
}
#
########################################

# this can be changed in case there's a request timeout and you need to restart somewhere
# in the middle of the operation,
# setting this offset to anything else than 0 will also disable the migration of issue links
# this can still be achieved through the link-issues-from-jira-to-gitlab.py script
INITIAL_OFFSET = 0

# set this to false if you manually want to call the issue link creation
# this can be handy if this script had to be interrupted for any reason
# issue links can still be migrated through the link-issues-from-jira-to-gitlab.py script
CREATE_ISSUE_LINKS = True

# set this to false if Jira / Gitlab is using a self-signed certificate.
VERIFY_SSL_CERTIFICATE = True

MAX_RESULTS = 50
GITLAB_URL = 'https://gitlab.com/'
GITLAB_PROJECT = '%s/%s' % (GITLAB_GROUPNAME, GITLAB_PROJECTNAME)

# variables to keep runtime state
JIRA_GITLAB_ISSUE_MAP = {}
ISSUE_LABELS_MAP = {}

if not GITLAB_PROJECT_ID:
    # find out the ID of the project.
    project_request = requests.get(
        GITLAB_URL + 'api/v4/projects?membership=true&simple=true&search=%s' % GITLAB_PROJECTNAME,
        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
        verify=VERIFY_SSL_CERTIFICATE
    )
    for project in project_request.json():
        if project['path_with_namespace'] == GITLAB_PROJECT:
            GITLAB_PROJECT_ID = project['id']
            break

if not GITLAB_PROJECT_ID:
    raise Exception('Unable to find %s in gitlab!' % GITLAB_PROJECT)

start_at = INITIAL_OFFSET

while True:
    jira_issues = requests.get(
        JIRA_URL + 'rest/api/2/search?jql=project=%s+ORDER+BY+created+ASC&maxResults=%d&startAt=%d' % (JIRA_PROJECT, MAX_RESULTS, start_at),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers={'Content-Type': 'application/json'}
    )

    if len(jira_issues.json()['issues']) == 0:
        break;

    offset = start_at
    for issue in jira_issues.json()['issues']:
        reporter = issue['fields']['reporter']['name']

        description = issue['fields']['description']
        if description == None:
            description = ''

        labels = issue['fields']['labels']
        labels.append(issue['fields']['issuetype']['name'])

        print('%d - %s - %s (%s) : %s' % (offset, issue['key'], reporter, issue['fields']['status']['statusCategory']['name'], issue['fields']['summary']))
        offset += 1

        weight = 0
        if issue['fields']['priority'] != None:
            weight_name = issue['fields']['priority']['name'];
            if weight_name != None and weight_name != 'Undefined':
                if weight_name == 'Blocker':
                    weight = 10
                elif weight_name == 'High':
                    weight = 8
                elif weight_name == 'Medium':
                    weight = 5
                elif weight_name == 'Low':
                    weight = 3
                elif weight_name == 'Lowest':
                    weight = 1

        assignee = ''
        if issue['fields']['assignee'] != None:
            assignee = issue['fields']['assignee']['name']

        assignee_id = None
        if assignee != '':
            gl_assignee = requests.get(
               GITLAB_URL + 'api/v4/user',
               headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(assignee, GITLAB_TOKEN)},
               verify=VERIFY_SSL_CERTIFICATE
            )
            print('> assignee : %s' % gl_assignee.text)
            assignee_id = gl_assignee.json()['id']

        gl_issue = requests.post(
            GITLAB_URL + 'api/v4/projects/%s/issues' % GITLAB_PROJECT_ID,
            headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(reporter, GITLAB_TOKEN)},
            verify=VERIFY_SSL_CERTIFICATE,
            data={
                'title': issue['fields']['summary'],
                'description': '%s\n\n-- Migrated from %sbrowse/%s' % (description, JIRA_URL, issue['key']),
                'labels': ','.join(labels),
                'assignee_id': assignee_id,
                'created_at': issue['fields']['created'],
                'updated_at': issue['fields']['updated']
            }
        )
        gl_issue_id = gl_issue.json()['iid']
        print('> created as ID %d : %s' % (gl_issue_id, gl_issue.text))

        # preserve a link between the JIRA and the GitLab issue IDs
        JIRA_GITLAB_ISSUE_MAP[issue['id']] = gl_issue_id
        ISSUE_LABELS_MAP[gl_issue_id] = labels

        # set issue weight if specified
        if weight != 0:
            gl_issue_weight = requests.put(
                GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, gl_issue_id),
                headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(reporter, GITLAB_TOKEN)},
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'weight': weight,
                    'updated_at': issue['fields']['updated']
                }
            )
            print('> weigh issue : %s' % gl_issue_weight.text)

        # add time tracking
        estimate = issue['fields']['aggregatetimeoriginalestimate']
        if estimate == None or estimate == 0:
            estimate = issue['fields']['timeestimate']
        if estimate != None and estimate != 0:
            requests.post(
                GITLAB_URL + 'api/v4/projects/%s/issues/%s/time_estimate' % (GITLAB_PROJECT_ID, gl_issue_id),
                headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'duration': '%ds' % estimate
                }
            )
        timespent = issue['fields']['aggregatetimespent']
        if timespent != None and timespent != 0:
            requests.post(
                GITLAB_URL + 'api/v4/projects/%s/issues/%s/add_spent_time' % (GITLAB_PROJECT_ID, gl_issue_id),
                headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'duration': '%ds' % timespent
                }
            )

        # get issue information
        issue_info = requests.get(
            JIRA_URL + 'rest/api/2/issue/%s/?fields=attachment,comment' % issue['id'],
            auth=HTTPBasicAuth(*JIRA_ACCOUNT),
            verify=VERIFY_SSL_CERTIFICATE,
            headers={'Content-Type': 'application/json'}
        ).json()

        # get attachments
        if len(issue_info['fields']['attachment']):
            for attachment in issue_info['fields']['attachment']:
                author = attachment['author']['name']

                _file = requests.get(
                    attachment['content'],
                    auth=HTTPBasicAuth(*JIRA_ACCOUNT),
                    verify=VERIFY_SSL_CERTIFICATE,
                )

                _content = BytesIO(_file.content)

                gl_upload = requests.post(
                    GITLAB_URL + 'api/v4/projects/%s/uploads' % GITLAB_PROJECT_ID,
                    headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(author, GITLAB_TOKEN)},
                    verify=VERIFY_SSL_CERTIFICATE,
                    files={
                        'file': (
                            attachment['filename'],
                            _content
                        )
                    },
                )
                print('> created attachment : %s' % gl_upload.text)

                del _content

                gl_attach = requests.post(
                    GITLAB_URL + 'api/v4/projects/%s/issues/%s/notes' % (GITLAB_PROJECT_ID, gl_issue_id),
                    headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(author, GITLAB_TOKEN)},
                    verify=VERIFY_SSL_CERTIFICATE,
                    data={
                        'body': gl_upload.json()['markdown'],
                        'created_at': attachment['created']
                    }
                )
                print('> commented attachment : %s' % gl_attach.text)

        for comment in issue_info['fields']['comment']['comments']:
            author = comment['author']['name']

            gl_comment = requests.post(
                GITLAB_URL + 'api/v4/projects/%s/issues/%s/notes' % (GITLAB_PROJECT_ID, gl_issue_id),
                headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(author, GITLAB_TOKEN)},
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'body': comment['body'],
                    'created_at': comment['created']
                }
            )
            print('> created comment : %s' % gl_comment.text)

        # close issue if status is done
        if issue['fields']['resolution'] != None:
            gl_issue_close = requests.put(
                GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, gl_issue_id),
                headers={'PRIVATE-TOKEN': GITLAB_USER_TOKENS.get(reporter, GITLAB_TOKEN)},
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'state_event': 'close',
                    'updated_at': issue['fields']['updated']
                }
            )
            print('> closed issue : %s' % gl_issue_close.text)

        print('')

    start_at += MAX_RESULTS

if INITIAL_OFFSET != 0 or CREATE_ISSUE_LINKS != True:
    print('Not creating issue links')
    exit()

# create issue links
print('Creating issue links')

start_at = 0

while True:
    jira_issues = requests.get(
        JIRA_URL + 'rest/api/2/search?jql=project=%s+ORDER+BY+created+ASC&maxResults=%d&startAt=%d' % (JIRA_PROJECT, MAX_RESULTS, start_at),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers={'Content-Type': 'application/json'}
    )
    start_at += MAX_RESULTS

    if len(jira_issues.json()['issues']) == 0:
        break;

    for issue in jira_issues.json()['issues']:

        for link in issue['fields']['issuelinks']:
            if 'outwardIssue' in link:
                source_jira_id = issue['id']
                target_jira_id = link['outwardIssue']['id']

                if source_jira_id in JIRA_GITLAB_ISSUE_MAP and target_jira_id in JIRA_GITLAB_ISSUE_MAP:
                    source_gitlab_id = JIRA_GITLAB_ISSUE_MAP[source_jira_id]
                    target_gitlab_id = JIRA_GITLAB_ISSUE_MAP[target_jira_id]

                    print('ISSUE ' + issue['key'] + ' ' + str(source_jira_id) + ' LINKS TO ' + str(target_jira_id) + ' (' + link['type']['name'] + ')')
                    gl_link = requests.post(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s/links' % (GITLAB_PROJECT_ID, source_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'target_project_id': GITLAB_PROJECT_ID,
                            'target_issue_iid': target_gitlab_id
                        }
                    )
                    print('> created link : %s' % gl_link.text)

                    source_labels = []
                    if source_gitlab_id in ISSUE_LABELS_MAP:
                        source_labels = ISSUE_LABELS_MAP[source_gitlab_id]
                    source_labels.append(link['type']['name'])
                    ISSUE_LABELS_MAP[source_gitlab_id] = source_labels
                    gl_link_source_labels = requests.put(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, source_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'labels': ','.join(source_labels)
                        }
                    )
                    print('> added link source label %s : %s' % (link['type']['name'], gl_link_source_labels.text))

                    target_labels = []
                    if target_gitlab_id in ISSUE_LABELS_MAP:
                        target_labels = ISSUE_LABELS_MAP[target_gitlab_id]
                    target_labels.append(link['type']['name'])
                    ISSUE_LABELS_MAP[target_gitlab_id] = target_labels
                    gl_link_target_labels = requests.put(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, target_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'labels': ','.join(target_labels)
                        }
                    )
                    print('> added link target label %s : %s' % (link['type']['name'], gl_link_target_labels.text))