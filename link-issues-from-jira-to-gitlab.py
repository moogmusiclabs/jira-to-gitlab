#!/usr/bin/python3

# Copyright 2019 Moog Music Inc. (https://www.moogmusic.com)
# Distributed under the MIT license, see LICENSE file

import requests
from requests.auth import HTTPBasicAuth
import re
from io import BytesIO

# this script was adapted from https://gist.github.com/toudi/67d775066334dc024c24

########################################
#
# FILL THESE IN FOR YOUR ACCOUNTS
#
JIRA_URL = 'https://my-org.atlassian.net/'
JIRA_ACCOUNT = ('jira-username', 'jira-password')
# the Jira project ID (short)
JIRA_PROJECT = 'PROJ'
# this token will be used whenever the API is invoked and
# the script will be unable to match the Jira's author of the comment / attachment / issue
# this identity will be used instead.
GITLAB_TOKEN = 'gitlab-token'
# the group in GitLab that the users and project belongs to
GITLAB_GROUPNAME = 'group-name'
# the project in GitLab that you are importing issues to.
GITLAB_PROJECTNAME = 'project-name'
# the numeric project ID. If you don't know it, the script will search for it
# based on the project name.
GITLAB_PROJECT_ID = None
#
########################################

# this can be changed in case there's a request timeout and you need to restart somewhere
# in the middle of the operation
INITIAL_OFFSET = 0

# set this to false if Jira / Gitlab is using a self-signed certificate.
VERIFY_SSL_CERTIFICATE = True

MAX_RESULTS = 50
GITLAB_URL = 'https://gitlab.com/'
GITLAB_PROJECT = '%s/%s' % (GITLAB_GROUPNAME, GITLAB_PROJECTNAME)

# variables to keep runtime state
JIRA_GITLAB_ISSUE_MAP = {}
ISSUE_LABELS_MAP = {}

if not GITLAB_PROJECT_ID:
    # find out the ID of the project.
    project_request = requests.get(
        GITLAB_URL + 'api/v4/projects?membership=true&simple=true&search=%s' % GITLAB_PROJECTNAME,
        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
        verify=VERIFY_SSL_CERTIFICATE
    )
    for project in project_request.json():
        if project['path_with_namespace'] == GITLAB_PROJECT:
            GITLAB_PROJECT_ID = project['id']
            break

if not GITLAB_PROJECT_ID:
    raise Exception('Unable to find %s in gitlab!' % GITLAB_PROJECT)

start_at = INITIAL_OFFSET

gl_issue_id = 0

print('Collecting issue information')
while True:
    jira_issues = requests.get(
        JIRA_URL + 'rest/api/2/search?jql=project=%s+ORDER+BY+created+ASC&maxResults=%d&startAt=%d' % (JIRA_PROJECT, MAX_RESULTS, start_at),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers={'Content-Type': 'application/json'}
    )

    if len(jira_issues.json()['issues']) == 0:
        break;

    offset = start_at
    for issue in jira_issues.json()['issues']:
        print('offset %d' % offset)
        print('%s : %s' % (issue['key'], issue['fields']['summary']))
        offset += 1
        gl_issue_id += 1

        gl_issue = requests.get(
            GITLAB_URL + 'api/v4/projects/%s/issues/%d' % (GITLAB_PROJECT_ID, gl_issue_id),
            headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
            verify=VERIFY_SSL_CERTIFICATE,
        )
        labels = gl_issue.json()['labels']
        print('> identified as ID %d : %s' % (gl_issue_id, str(labels)))

        # preserve a link between the JIRA and the GitLab issue IDs
        JIRA_GITLAB_ISSUE_MAP[issue['id']] = gl_issue_id
        ISSUE_LABELS_MAP[gl_issue_id] = labels

        print('')

    start_at += MAX_RESULTS

# create issue links
print('Creating issue links')

start_at = INITIAL_OFFSET

while True:
    jira_issues = requests.get(
        JIRA_URL + 'rest/api/2/search?jql=project=%s+ORDER+BY+created+ASC&maxResults=%d&startAt=%d' % (JIRA_PROJECT, MAX_RESULTS, start_at),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers={'Content-Type': 'application/json'}
    )
    start_at += MAX_RESULTS

    if len(jira_issues.json()['issues']) == 0:
        break;

    for issue in jira_issues.json()['issues']:

        for link in issue['fields']['issuelinks']:
            if 'outwardIssue' in link:
                source_jira_id = issue['id']
                target_jira_id = link['outwardIssue']['id']

                if source_jira_id in JIRA_GITLAB_ISSUE_MAP and target_jira_id in JIRA_GITLAB_ISSUE_MAP:
                    source_gitlab_id = JIRA_GITLAB_ISSUE_MAP[source_jira_id]
                    target_gitlab_id = JIRA_GITLAB_ISSUE_MAP[target_jira_id]

                    print('ISSUE ' + issue['key'] + ' ' + str(source_jira_id) + ' LINKS TO ' + str(target_jira_id) + ' (' + link['type']['name'] + ')' + ' GITLAB (' + str(source_gitlab_id) + ' TO ' + str(target_gitlab_id) + ')')
                    gl_link = requests.post(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s/links' % (GITLAB_PROJECT_ID, source_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'target_project_id': GITLAB_PROJECT_ID,
                            'target_issue_iid': target_gitlab_id
                        }
                    )
                    print('> created link : %s' % gl_link.text)

                    source_labels = []
                    if source_gitlab_id in ISSUE_LABELS_MAP:
                        source_labels = ISSUE_LABELS_MAP[source_gitlab_id]
                    source_labels.append(link['type']['name'])
                    ISSUE_LABELS_MAP[source_gitlab_id] = source_labels
                    gl_link_source_labels = requests.put(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, source_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'labels': ','.join(source_labels)
                        }
                    )
                    print('> added link source label %s : %s' % (link['type']['name'], gl_link_source_labels.text))

                    target_labels = []
                    if target_gitlab_id in ISSUE_LABELS_MAP:
                        target_labels = ISSUE_LABELS_MAP[target_gitlab_id]
                    target_labels.append(link['type']['name'])
                    ISSUE_LABELS_MAP[target_gitlab_id] = target_labels
                    gl_link_target_labels = requests.put(
                        GITLAB_URL + 'api/v4/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, target_gitlab_id),
                        headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
                        verify=VERIFY_SSL_CERTIFICATE,
                        data={
                            'labels': ','.join(target_labels)
                        }
                    )
                    print('> added link target label %s : %s' % (link['type']['name'], gl_link_target_labels.text))