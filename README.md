# Jira To GitLab

Scripts to migrate issues from Jira with REST API v2 to GitLab with REST API v4:
* https://developer.atlassian.com/cloud/jira/platform/rest/v2/
* https://docs.gitlab.com/ee/api/

These scripts are distributed under the MIT license and provided as-is, more information can be found in the LICENSE file.

We've recently migrated all our issues from Jira to GitLab and wrote these scripts to preserve as much of the information as possible. In order for these scripts to work, you'll need to fill information into the variables at the top of each file.

The main file is `move-issues-from-jira-to-gitlab.py`, before anything can function you'll have to set the following variables:

* `JIRA_URL`
* `JIRA_ACCOUNT`
* `JIRA_PROJECT`
* `GITLAB_TOKEN`
* `GITLAB_GROUPNAME`
* `GITLAB_PROJECTNAME`
* `GITLAB_USER_TOKENS`

We recommend creating a dummy GitLab project first into which you test the issue migration. That project can easily be deleted together with all its issues. If you do need to delete all the issues from an existing GitLab project and perform the migration again, you can use the `delete-issues-from-gitlab.py` script. Note that this deletes all your project's issues without a warning, be very careful when using the `delete` script.

The `move` script will create a new issue for each one that can be found in the configured Jira project.

The following information will be transferred:

* Title
* Description (with added link to original Jira issue)
* Reporter
* Labels
* Creation Timestamp
* Update Timestamp
* Assignee
* Resolution
* Priority (as Weight from 0-10)
* Estimate
* Time Spent
* Comments
* Attachments

After all the issues have been migrated, links between the issues are preserved and labels are added to indicate which type of links apply to each issue. This process requires the script to move all the issues in a single run because it tracks which Jira issue ID maps to the newly created GitLab issue ID.

If for any reason the `move` script can't be completed in a single run (request timeouts, network interruptions, ...), it can be relaunched with a different value for `INITIAL_OFFSET`. The links will then not be created automatically. Through the `link-issues-from-jira-to-gitlab.py` script, the same can be achieved as long as no changes have been made to the issues in either Jira or GitLab in the meantime.